FROM nginx:alpine

COPY app.conf /etc/nginx/nginx.conf

WORKDIR /usr/src/app
COPY build/ /usr/src/app

EXPOSE 8080